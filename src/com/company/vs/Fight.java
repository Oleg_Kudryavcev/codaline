package com.company.vs;

import com.company.animals.Cat;
import com.company.animals.Dog;

public class Fight implements IFight {

    private Cat cat;
    private Dog dog;
    private String winner;

    {
        cat = new Cat();
        dog = new Dog();
    }

    @Override
    public void fight() {
        this.winner = cat.hashCode() > dog.hashCode() ? cat.getClass().getSimpleName() : dog.getClass().getSimpleName();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Winner is " + this.winner);
    }
}
