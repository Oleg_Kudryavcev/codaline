package com.company.runner;

import com.company.util.exceptions.HeartLegException;
import com.company.animals.Animal;
import com.company.animals.Cat;
import com.company.factory.AnimalFactory;
import com.company.vs.Fight;

public class Main {

    private static int select;
    private static Animal myAnimal;
    private static boolean defaultFlow = false;

    static {
        select = (int) (Math.random() * 2 + 1);
        myAnimal = AnimalFactory.getAnimal(select);
    }

    public static void main(String[] args) {
        if (defaultFlow) {
            defaultAnimalFlow();
        } else {
            fightFlow();
        }
    }

    private static void defaultAnimalFlow() {
        System.out.println("Your animal is " + myAnimal.getClass().getSimpleName().toLowerCase());
        String animalName = myAnimal instanceof Cat ? "Murzik" : "Sharik";
        myAnimal.setName(animalName);
        System.out.println("Your animal name is " + myAnimal.getName());
        while (true) {
            try {
                myAnimal.play();
                myAnimal.eat();
                myAnimal.sleep(1);
            } catch (HeartLegException e) {
                System.out.println("Your animal damage his leg");
                if (myAnimal instanceof Cat) {
                    Cat cat = (Cat) myAnimal;
                    Integer catsLives = cat.getCatsLife();
                    System.out.println("Your cat has " + catsLives + " lives");
                    if (!cat.isAlive()) {
                        System.out.println("Game over");
                        break;
                    }
                }
            }
        }
    }

    private static void fightFlow() {
        try (Fight f = new Fight()) {
            f.fight();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
