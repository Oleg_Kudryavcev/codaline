package com.company.util;

public enum AnimalType {
    DOG(1),
    CAT(2);

    private int value;

    private AnimalType(int value) {
        this.value = value;
    }

    public static AnimalType fromValue(int val) {
        if (val == 1) {
            return AnimalType.DOG;
        } else if (val == 2) {
            return AnimalType.CAT;
        } else {
            throw new IllegalArgumentException("Can't recognise animal");
        }
    }

}
