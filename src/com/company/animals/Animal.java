package com.company.animals;

import com.company.util.exceptions.EmptyNameException;

import java.util.concurrent.TimeUnit;

public abstract class Animal implements IAnimal {

    protected String name;

    @Override
    public void sleep(int delay) {
        try {
            System.out.println("Sleep");
            TimeUnit.SECONDS.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void setName(String name) {
        if (name != null && !name.isEmpty()) {
            this.name = name;
        } else {
            throw new EmptyNameException("Name cannot be empty");
        }
    }

    public String getName() {
        return this.name;
    }

}
