package com.company.animals;

import com.company.util.exceptions.HeartLegException;

public class Cat extends Animal {

    private Integer catsLife = 9;
    private Integer fatness = 0;

    @Override
    public void eat() {
        System.out.println("Cat eat");
        this.fatness += 10;
        if (this.fatness == 100) {
            System.out.println("Your cat is very fat. And he lost 1 life");
            this.catsLife--;
            this.fatness = 0;
        }
    }

    @Override
    public void play() throws HeartLegException {
        int possibility = (int) (Math.random() * 5 + 1);
        if (possibility > 4) {
            catsLife--;
            throw new HeartLegException("Cat heart his leg");
        } else {
            System.out.println("Cat is playing");
        }
    }

    public Integer getCatsLife() {
        return this.catsLife;
    }

    public boolean isAlive() {
        return this.getCatsLife() != 0;
    }

}
